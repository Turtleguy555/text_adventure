package items;

import textadventure.World;

public class Boat extends Item{

    public Boat(World world, String name, int weight, boolean takeable, String description) {
        super(world, name, weight, takeable, description);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void doUse() {
        if(getWorld().getPlayer().getCurrentRoom().getName().equals("DOCK")) {
            if(getWorld().getPlayer().getHealth() > 99) {
                World.print("You drag your boat onto the dock and set sail. It takes many days, but you finaly make it onto the mainland. As it turns out, you arrive in Honolulu, Hawaii. You take next few days to relax and forget about the tramatic plane crash.\n\n");
                World.print("You win!");
                System.exit(0);
            }else {
                World.print("You drag your boad onto the dock and set sail. While sailing, you start to starve. Eventually, you become so malnourished that you die!\n\n)");
                World.print("Game Over!");
                System.exit(0);
            }
        }else {
            World.print("You can't set sail here!\n\n");
        }
        
    }

}
