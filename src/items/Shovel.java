package items;

import textadventure.Room;
import textadventure.World;

public class Shovel extends Item {

    public Shovel(World world, String name, int weight, boolean takeable, String description) {
        super(world, name, weight, takeable, description);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void doUse() {
        if(getWorld().getPlayer().getCurrentRoom().getName().equals("BEACH")) {
            World.print("You dig a hole in the beach. \n\n");
            getWorld().getPlayer().getCurrentRoom().setDescription("A dazzling and shiny beach. A hole is located in the north. \n\n");
            getWorld().setRoomHole();
        }else {
            World.print("You can't dig here. \n\n");
        }
        
    }

}

