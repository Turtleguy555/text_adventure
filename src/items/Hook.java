package items;

import textadventure.World;

public class Hook extends Item{

    public Hook(World world, String name, int weight, boolean takeable, String description) {
        super(world, name, weight, takeable, description);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void doUse() {
        if(getWorld().getPlayer().getCurrentRoom().getName().equals("SHALLOW_SHORE")) {
            World.print("You cast your fishing rod in the water and manage to catch a fish!. \n\n");
            getWorld().getPlayer().getCurrentRoom().addItem(new Food(getWorld(), "fish" , 2 , Item.TAKEABLE, "A fish!",true));
        }else if(getWorld().getPlayer().getCurrentRoom().getName().equals("CLIFF") && getWorld().getPlayer().getMap() == false){
            World.print("You cast your fishing rod over the edge of the cliff and manage to hook something up! It appears to be a piece of paper. \n\n");
            getWorld().getPlayer().getCurrentRoom().addItem(new Map(getWorld(), "paper" , 0 , Item.TAKEABLE, "|____/-x"));
            getWorld().getPlayer().setMap(true);
        }else {
            World.print("You cast your fishing rod but nothing happens");
        }
        
    }
    

}

