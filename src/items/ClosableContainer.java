package items;

import interfaces.Closeable;
import textadventure.World;

public class ClosableContainer extends Container implements Closeable{
    boolean open;
    String n;
    public ClosableContainer(World world, String name, int weight, boolean takeable, String description, boolean isOpen) {
        super(world, name, weight, takeable, description);
        // TODO Auto-generated constructor stub
        open = isOpen;
        n = name;
    }
    
    public boolean isOpen() {
        return open;
    }
    
    public void doOpen() {
        open = true;
        World.print(getName() +" is now opened." + "\n\n");
    }

    /** Closes this Closeable object */
    public void doClose() {
        open = false;
        World.print(getName() +" is now closed." + "\n\n");
    }
    
    public void doExamine() {   
        if(open == false) {
            World.print("The " + n +" is closed." + "\n\n");
        }else {
            World.print(getDescription() + " Inside the " + getName() + " you see " + getItemString() + ".\n\n");       
        
        }
        
    }
    
    public Item doTake(Item item) {
        if(open == true) {
            getWorld().getPlayer().addItem(item);
            this.removeItem(item);      
            World.print("Done!\n\n"); // meaning you put [item] into [container]
            return item;
        }else {
            World.print("Its closed" + "\n\n");
            return null;
        }
        
        
    }
    
    public Item doPut(Item item, Container source) {
        if(open == true) {
            addItem(item);
            source.removeItem(item);        
            World.print("Done!\n\n"); // meaning you put [item] into [container]
            return item;
        }else {
            World.print("Its closed");
            return null;
        }
        
    }
    
    
}





