package items;

import interfaces.Edible;
import textadventure.World;

public class Food extends Item implements Edible {
    String n;
    String descrip;
    boolean food;
    public Food(World world, String name, int weight, boolean takeable, String description, boolean edible) {
        super(world, name, weight, takeable, description);
        // TODO Auto-generated constructor stub
        n = name;
        descrip = description;
        food = edible;
    }
    
    public void doEat() {
        World.print("You eat the " + n + " and felt stronger!"+"\n\n");
        getWorld().getPlayer().setHealth(getWeight());
        getWorld().getPlayer().setTeeth(false);
    }
    
    public boolean isEdible() {
        return food;
    }
    
    public void doUse() {
        doEat();
    }
    

}
