package items;

import textadventure.World;

public class Match extends Item{

    public Match(World world, String name, int weight, boolean takeable, String description) {
        super(world, name, weight, takeable, description);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void doUse() {
        if(getWorld().getPlayer().getCurrentRoom().getName().equals("SMALL_HOLE")) {
            if(getWorld().getPlayer().getCurrentRoom().getItem("piles_of_wood") != null && getWorld().getPlayer().getCurrentRoom().hasItem(getWorld().getPlayer().getCurrentRoom().getItem("piles_of_wood"))) {
                World.print("You light the wood on fire \n\n");
                getWorld().getPlayer().getCurrentRoom().removeItem("piles_of_wood");
                
                getWorld().getPlayer().setFire(true);
                getWorld().getPlayer().getCurrentRoom().setDescription("There is now a small fire in the hole \n\n");
            }else {
                World.print("You make a small spark but nothing happens \n\n");
            }
        }else {
            World.print("You make a small spark but nothing happens \n\n");
        }
        
    }

}

