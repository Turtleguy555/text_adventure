package items;

import java.util.Arrays;

import interfaces.Lockable;
import textadventure.World;

public class Safe extends ClosableContainer implements Lockable{
    boolean isLocked = true;
    public Safe(World world, String name, int weight, boolean takeable, String description, boolean isOpen, boolean isLocked) {
        super(world, name, weight, takeable, description, isOpen);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean isLocked() {
        // TODO Auto-generated method stub
        return isLocked;
    }

    @Override
    public void doLock() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void doUnlock() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void doExamine() {   
        if(isLocked == true) {
            World.print("The " + n +" is locked." + "\n\n");
        }else {
            World.print(getDescription() + " Inside the " + getName() + " you see " + getItemString() + ".\n\n");       
        
        }
        
    
    }
    
    
}

