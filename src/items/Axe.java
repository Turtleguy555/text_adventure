package items;

import textadventure.World;

public class Axe extends Item{

    public Axe(World world, String name, int weight, boolean takeable, String description) {
        super(world, name, weight, takeable, description);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void doUse() {
        if(getWorld().getPlayer().getCurrentRoom().getName().equals("DENSE_JUNGLE") && getWorld().getPlayer().getJ() == false) {
            World.print("You chop down all the trees leaving piles_of_wood \n\n");
            getWorld().getPlayer().getCurrentRoom().addItem(new Wood(getWorld(), "piles_of_wood" , 0 , Item.TAKEABLE, "The souls of the trees will forever haunt this island.\n\n"));
            getWorld().getPlayer().getCurrentRoom().setDescription("A barren jungle void of any flora or fauna except for a lonely coconut tree. \n\n");
            getWorld().getPlayer().setJ(true);
            
            
        }else if(getWorld().getPlayer().getCurrentRoom().getName().equals("TROPICAL_FOREST") && getWorld().getPlayer().getF() == false){
            World.print("You chop down all the trees leaving piles_of_wood \n\n");
            getWorld().getPlayer().getCurrentRoom().addItem(new Wood(getWorld(), "piles_of_wood" , 0 , Item.TAKEABLE, "The souls of the trees will forever haunt this island.\n\n"));
            getWorld().getPlayer().getCurrentRoom().setDescription("A barren forest void of any flora or fauna except for a lonely redwood_tree. \n\n");
            getWorld().getPlayer().setF(true);
            
            
        }else {
            World.print("It would not be wise to swing your axe here \n\n");
        }
        
    }

}

