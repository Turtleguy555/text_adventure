package items;

import textadventure.World;

public class Pan extends Container {

    public Pan(World world, String name, int weight, boolean takeable, String description) {
        super(world, name, weight, takeable, description);
        // TODO Auto-generated constructor stub
    }
    
    public void doUse() {
        if(getWorld().getPlayer().isFire() == true && getWorld().getPlayer().getCurrentRoom().getName().equals("SMALL_HOLE") ) {
            if(getItem("pineapple") != null && getItem("fish") != null && getItem("coconut") != null) {
                World.print("You created a gourmet meal! Fish ala fancy!");
                addItem(new Food(getWorld(), "fancy_fish" , 100 , Item.TAKEABLE , "A deliciously nutrious fish meal. Gordon Ramsey would be proud!", true));
                removeItem("coconut");
                removeItem("fish");
                removeItem("pineapple");
            }else if(getItem("pineapple") == null && getItem("fish") == null && getItem("coconut") == null) {
                World.print("Your pan gets hot but nothing happens \n\n");
                
            }else if(getItem("pineapple") == null || getItem("fish") == null || getItem("coconut") == null) {
                World.print("The food you make tastes really bad. You realize that starving would be a kinder fate than eating your mess \n\n");
            }
        }else {
            World.print("You can't use your pan");
        }
        
    }

}
