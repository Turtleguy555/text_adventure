package textadventure;

public class Hole extends Room{
    private boolean isFire;
    public Hole(String name, String description, boolean isLocked, World world) {
        super(name, description, isLocked, world);
        isFire = false;
        // TODO Auto-generated constructor stub
    }
    
    public boolean getFire() {
        return isFire;
    }
    
    public void setFire(boolean l) {
        isFire = l;
    }
}

