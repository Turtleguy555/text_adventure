package textadventure;

public class OutsideRoom extends Room{

    public OutsideRoom(String name, String description, boolean isLocked, World world) {
        super(name, description, isLocked, world);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void doGo() {
        World.print("Your carpool has arrived \n\n");
        if(getWorld().getPlayer().getTeeth() == false) {
            World.print("Your friends think your breath smells. Game Over!");
            System.exit(0);
        }
        
        if(getWorld().getPlayer().getHealth()  == 2) {
            World.print("You starve to death. Game Over!");
            System.exit(0);
        }
        
        if(getWorld().getPlayer().getClothes()  == false) {
            World.print("You die of embarassment. Game Over!");
            System.exit(0);
        }
        
        
        World.print("You win");
        System.exit(0);
        
    }   

}
