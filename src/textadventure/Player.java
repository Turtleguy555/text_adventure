package textadventure;

import items.Container;

/**
 * A Player can contain objects and knows the Room it's currently in.
 */
public class Player extends Container {
    private int health;
    private boolean hasBrushedTeeth;
    private boolean clothes;
    private boolean foundBlue;
    private boolean isFire;
    private boolean isOver;
    private boolean isChopFor;
    private boolean isChopJun;
    /** Room this Player is in at the moment */
    private Room currentRoom;
    
    /** Creates a player that starts in the given Room with no items */
    public Player(Room startRoom, World world) {
        super(world, "Player", "You are looking good today!");
        currentRoom = startRoom;
        health = 0;
        hasBrushedTeeth = false;
        clothes = false;
        foundBlue = false;
        isFire = false;
        isChopFor = false;
        isChopJun = false;
    }
    
    
    

    /* Getters and setters */
    public boolean getF() {
        return isChopFor;
    }
    public void setF(boolean l) {
        isChopFor = l;
    }
    
    public boolean getJ() {
        return isChopJun;
    }
    public void setJ(boolean l) {
        isChopJun = l;
    }

    public void setSail(boolean l) {
        isOver = l;
    }
    
    public boolean isSail() {
        return isOver;
    }
    public boolean isFire() {
        return isFire;
    }
    
    public void setFire(boolean l) {
        isFire = l;
    }
    public boolean getMap() {
        return foundBlue;
    }
    public void setMap(boolean l) {
        foundBlue = l;
    }
    
    public void setClothes(boolean c) {
        clothes =c;
    }
    
    public boolean getClothes() {
        return clothes;
    }
    public void setTeeth(boolean t) {
        hasBrushedTeeth = t;
    }
    
    public boolean getTeeth() {
        return hasBrushedTeeth;
    }
    
    public int getHealth() {
        return health;
    }

    
    public void setHealth(int l) {
        health = health + l;
    }
    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room newRoom) {
        currentRoom = newRoom;
    }
}
