
package textadventure;

import java.util.Calendar;




import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;

import command.Command;
import items.Axe;
import items.ClosableContainer;

import items.Container;
import items.Food;

import items.Hook;
import items.Item;
import items.Map;
import items.Match;
import items.Pan;
import items.Safe;
import items.Scenery;
import items.Shovel;

import items.Tree;
import items.UselessItem;

public class World {

    /* Room String constants
     * 
     * Instead of hard-coding everywhere, room names are defined here.
     * They're also public and static so they can be accessed
     * anywhere using World.stringName
     */
    public static final String BEACH = "BEACH";
    public static final String FOREST = "TROPICAL_FOREST";
    public static final String JUNGLE = "DENSE_JUNGLE";
    public static final String DOCK = "DOCK";
    public static final String CLIFF = "CLIFF";
    public static final String SHORE = "SHALLOW_SHORE";
    public static final String HOLE = "SMALL_HOLE";

    /** Line word wrap length for console output */
    public static final int LINE_WRAP_LENGTH = 70;

    /* private World attributes */  
    /** The main player of the game, which is you */
    private Player player;
    
    /** Keeps track of the Rooms in this World using <Key, Value>
     *  pairs of <RoomName, RoomObject>
     */
    private HashMap<String, Room> rooms;

    /** Reads user input */
    private static final Scanner IN = new Scanner(System.in);

    /** Keeps track of time using built-in Java object */
    private Calendar localTime;
    long startMilliseconds;

    /**
     * Create the game and initialize its internal map.
     */
    public World() {
        initializeNewGame();
    }

    public void initializeNewGame() {
        // Initialize attributes
        createRooms();
        player = new Player(rooms.get(BEACH), this);
       

        // Start game timer 
        localTime = new GregorianCalendar(2018, Calendar.AUGUST, 14, 7, 15, 0); // August 14, 2018 7:15 A.M.
        startMilliseconds = System.currentTimeMillis();
    }
    
    /**
     * Print out the opening message for the player.
     */
    private void printWelcome() {
        print("You are stranded on an island after an emergency plane crash. Everyone else is dead and debris from the crash is littered throughout the island. \n\n");
        
        print("Type 'help' if you need help.\n\n");
        print(player.getCurrentRoom().getDescription());
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms() {

        // Create a new HashMap to store the rooms in
        rooms = new HashMap<String, Room>();

        // Create rooms and add them to our HashMap
        rooms.put(BEACH, new Room(BEACH, "A dazzling and shiny beach. \n\n", this));
        rooms.put(FOREST, new Room(FOREST, "A quite and mysterious forest filled with trees. \n\n", this));
        rooms.put(JUNGLE, new Room(JUNGLE, "A dense jungle filled with all sorts of plants and animals. \n\n", this));
        rooms.put(DOCK, new Room(DOCK, "An abandoned dock. \n\n", this));
        rooms.put(CLIFF, new Room(CLIFF, "A steep cliff hangs out in front of you. Something below catches your eye, but you can't reach it... \n\n", this));
        rooms.put(SHORE, new Room(SHORE, "A small little shore. The perfect place to swim around and relax. \n\n", this));
        
        
        
        
        // Define room exits.  Order is north, east, south, west
        rooms.get(BEACH).setExits(null , null, null, rooms.get(FOREST));
        rooms.get(FOREST).setExits(rooms.get(CLIFF), rooms.get(BEACH), null , rooms.get(JUNGLE));
        rooms.get(JUNGLE).setExits(null, rooms.get(FOREST), rooms.get(SHORE), rooms.get(DOCK));
        rooms.get(DOCK).setExits(null, rooms.get(JUNGLE), null, null);
        rooms.get(CLIFF).setExits(null, null, rooms.get(FOREST), null);
        rooms.get(SHORE).setExits(rooms.get(JUNGLE), null, null, null);
        
        // Create game items and add them to rooms
    
        Pan pan = new Pan(this, "pan" , 0, Item.TAKEABLE, "A state of the art frying pan.");
        Shovel shovel = new Shovel(this, "shovel" , 0, Item.TAKEABLE, "A rusty, badly damaged, shovel");
        Container safe = new Container(this, "safe" , 0, Item.NOT_TAKEABLE, "A broken safe. Its doors hang wide open for you to observe.");//true,false
        Container cocoTree = new Container(this, "coconut_tree", 0 , Item.NOT_TAKEABLE, "A tall tropical coconut tree.");
        Container pineBush = new Container(this, "pineapple_bush" ,0, Item.NOT_TAKEABLE, "A weird patch of grass hides a fruity gem." );
        Food coconut = new Food(this, "coconut",1, Item.TAKEABLE, "A huge coconut! ",true);
        Food pineapple = new Food(this, "pineapple",1, Item.TAKEABLE, "A huge coconut! ",true);
        Hook hook = new Hook(this, "fishing_rod" , 0, Item.TAKEABLE, "An abandoned fishing hook");
        Tree tree = new Tree(this, "redwood_tree" , 0 , Item.NOT_TAKEABLE, "A lonely redwood living in a world with other generic trees");
        
        Axe axe = new Axe(this, "axe" , 0, Item.TAKEABLE , "A well sharpened and loved axe.");
        Match matches = new Match(this, "matches", 0 , Item.TAKEABLE, "a box of matches");
        
        rooms.get(FOREST).addItem(safe);
        safe.addItem(pan);
        rooms.get(DOCK).addItem(axe);
        rooms.get(FOREST).addItem(tree);
        rooms.get(SHORE).addItem(hook);
        rooms.get(BEACH).addItem(shovel);
        rooms.get(JUNGLE).addItem(cocoTree);
        rooms.get(CLIFF).addItem(pineBush);
        cocoTree.addItem(coconut);
        pineBush.addItem(pineapple);
        safe.addItem(matches);
        
        
        

        
        
    }
    
    public void setRoomHole() {
        rooms.put(HOLE, new Room(HOLE, "A shallow humble hole. \n\n", this));
        rooms.get(BEACH).setExits(rooms.get(HOLE) , null, null, rooms.get(FOREST));
        rooms.get(HOLE).setExits(null , null, rooms.get(BEACH), null);
    }

    public Room getRoom(String key) {
        return rooms.get(key);
    }

    public Player getPlayer() {
        return player;
    }
    
    
    /**
     * Main play routine. Loops until end of play.
     */
    public void play() {
        printWelcome();
        
        // Enter the main command loop. Here we repeatedly read commands and
        // execute them until the game is over.
        boolean finished = false;
        while (!finished) {
            System.out.print("> "); // print prompt
            String command = IN.nextLine();
            finished = processCommand(command);
            
        }
        print("\nThank you for playing!  Good bye!\n\n");
    }

    /**
     * Updates and prints out the current game time. Every second spent between game
     * commands becomes a second spent within the game.
     */
    
    /**
     * Given a command, process (that is: execute) the command. If this command ends
     * the game, true is returned, otherwise false is returned.
     */
    private boolean processCommand(String command) {
        StringTokenizer tok = new StringTokenizer(command);
        if (tok.hasMoreTokens()) {
            if (tok.nextToken().equals("quit"))
                return true;
            else if (!Command.hasValidCommandWord(command)) {
                print("I don't know the command '" + command + "'\n\n");
                return false;
            }
            else {
                Command.doCommand(command, this);
                return false;
            }
        } else {
            print("Please type in a command" + "\n\n");
            return false;
        }
        
    }

    /**
     * Helper method to print any String in a line-wrapped format.
     * Prints the input String line-wrapped to a column width of LINE_WRAP_LENGTH,
     * which is a constant defined at the top of this class.
     * 
     * Pseudocode and strategy:
     * There are so many special cases, I could not have written this without planning it out.
     * I decided to leave the comments here so you can see the strategy.
     * 
     * 
     * while(length of str >= lengthLimit)
     *      Find the first occurrence of \n.
     *      If it's < lengthLimit then add substring(0, occurrence + 1) to output and reduce str by same amount.
     *      Else if there's a space at lengthLimit then add substring(0, lengthLimit) to output and
     *          reduce str by same amount
     *      Else find last occurrence of space within substring(0, lengthLimit)
     *          If no space anywhere then
     *              If there's a space at least somewhere in str, then add substring(0, firstSpace) to
     *                  output and reduce str by same amount
     *              Else (no space anywhere)
     *                  add rest of str to output and reduce by same amount
     *          Else (space somewhere within substring)
     *              add str.substring(0, index of last space) to output
     *              reduce str by same amount
     * If there's anything left in str, add it.
     */
    public static void print(String str) {
        String output = "";
        
        while (str.length() >= LINE_WRAP_LENGTH) {
            int lineBreakIndex = str.indexOf("\n");
            if (lineBreakIndex < LINE_WRAP_LENGTH && lineBreakIndex != -1) {
                output += str.substring(0, lineBreakIndex + 1);
                str = str.substring(lineBreakIndex + 1);
            }
            else if (str.charAt(LINE_WRAP_LENGTH) == ' ') {
                output += str.substring(0, LINE_WRAP_LENGTH);
                str = str.substring(LINE_WRAP_LENGTH + 1);
                if (str.length() > 0)
                    output += "\n";
            }
            else {
                int lastSpaceIndex = str.substring(0, LINE_WRAP_LENGTH).lastIndexOf(" ");
                if (lastSpaceIndex == -1) {
                    int firstSpaceIndex = str.indexOf(" ");
                    if (firstSpaceIndex != -1) {
                        output += str.substring(0, firstSpaceIndex);
                        str = str.substring(firstSpaceIndex + 1);
                        if (str.length() > 0)
                            output += "\n";
                    }
                    else {
                        output += str;
                        str = "";
                    }
                }
                else {
                    output += str.substring(0, lastSpaceIndex);
                    str = str.substring(lastSpaceIndex + 1);
                    if (str.length() > 0)
                        output += "\n";
                }
            }
        }
        if (str.length() > 0) {
            output += str;
        }
        System.out.print(output);
    }
}