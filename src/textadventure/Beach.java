package textadventure;

public class Beach extends Room{
    boolean isHole;
    public Beach(String name, String description, boolean isLocked, World world) {
        super(name, description, isLocked, world);
        // TODO Auto-generated constructor stub
        isHole = false;
    }
    
    public void setHole(boolean l) {
        isHole = l;
    }
    
    public boolean getHole() {
        return isHole;
    }
    

}
