package command;

import command.Command;
import items.Container;
import items.Item;
import textadventure.World;

public class CommandPut extends Command {

    @Override
    public String[] getCommandWords() {
        return new String[]{"put"};
    }

    @Override
    public void doCommand(String cmd, String[] params, World world) {
      try {
          if(params.length == 0) {
              World.print("I don't understand. \n\n");
              return;
  
          }
          String item = params[0];
          Item container = world.getPlayer().getCurrentRoom().getItem(params[2]);
          
          if(params.length != 3) {
              World.print("Invalid syntax \n\n");
          }else if(!(world.getPlayer().getCurrentRoom().hasItem(item) || world.getPlayer().hasItem(item))) {
              World.print("You can't see any " + item +" here \n\n");
              
          }else if(!((Container)container instanceof Container)) {
              World.print("The " + container + " can't hold things. \n\n");
          }else if(params[0].equals(params[2])){
              World.print("You can't put the " + item + " into itself! \n\n");
          }
          
          if(params.length > 0 && world.getPlayer().hasItem(item)) {
              
              ((Container)container).doPut(world.getPlayer().getItem(item),world.getPlayer());
          }else {
              ((Container)container).doPut(world.getPlayer().getItem(item), world.getPlayer().getCurrentRoom());   
          }
      }catch(ArrayIndexOutOfBoundsException e) {
        World.print("Invalid command \n\n");
      }catch(NullPointerException i) {
        World.print("Invalid command \n\n" );
      }

    }

    @Override
    public String getHelpDescription() {
        return " [item] into [container]";
    }

}

