package command;

import command.Command;
import interfaces.Closeable;
import items.Boat;
import items.Item;
import items.Map;
import textadventure.World;
import java.util.*;

public class CommandBuild extends Command {

    @Override
    public String[] getCommandWords() {
        return new String[]{"build", "make"};
    }

    @Override
    public void doCommand(String cmd, String[] params, World world) {
        
        
        if(params.length != 1) {
            world.print("I don't understand what your trying to do. \n\n");
            return;
        }
            
            if(world.getPlayer().getCurrentRoom().getName().equals("BEACH")) {
                if(world.getPlayer().hasItem("piles_of_wood")) {
                    if (params[0].equals("|____/-x")) {
                
                    
                        world.getPlayer().getCurrentRoom().removeItem("piles_of_wood");
                        world.getPlayer().getCurrentRoom().addItem(new Boat(world, "boat" , 0 , Item.TAKEABLE, "|____/-x"));
                        world.print("You have built a boat! \n\n");
                    }else {
                        world.print("Your creation has no purpose, so you tear it down. \n\n");
                    }
                }else {
                    world.print("You don't have the right supplies to build with. \n\n");
                }
            }else {
                world.print("You can't build here \n\n");
            }
        
            

            
            
            
    }
    
    @Override
    public String getHelpDescription() {
        return "[string_of_characters]";
                
    }
}

