package command;

import command.Command;
import items.Container;
import items.Item;
import textadventure.World;

public class CommandTake extends Command {

    @Override
    public String[] getCommandWords() {
        return new String[]{"take", "get", "grab", "hold"};
    }

    @Override
    public void doCommand(String cmd, String[] params, World world) {
        if (params.length == 1) {
            String itemName = params[0];
            if (world.getPlayer().getCurrentRoom().hasItem(itemName)) {
                Item item = world.getPlayer().getCurrentRoom().getItem(itemName);
                item.doTake(world.getPlayer().getCurrentRoom());
            }
            else if (world.getPlayer().hasItem(itemName))
                World.print("You already have that!\n\n");
            else
                World.print("You can't see any " + itemName + " here.\n\n");
        }else if(params.length == 3) {
            String contName = params[2];
            String item = params[0];
            Item container = world.getPlayer().getCurrentRoom().getItem(contName);
            if(!params[1].equals("from")) {
                World.print("I don't understand \n\n");
            }else if(world.getPlayer().getCurrentRoom().hasItem(contName) == false ) {
                World.print("You can't see any " + contName + " here. \n\n");
            }else if(!(container instanceof Container )){
                World.print("The " + contName + " can't hold things! \n\n");
            }else if(!((Container)container).hasItem(item)) {
                World.print("The " + contName + "doesn't have a " + item+ "\n\n");
            }else {
                Item i = ((Container)container).getItem(item);
                ((Container)container).doTake(i);
            }
                
            
            
        }
        else {
            World.print("I don't understand.\n\n");
        }
    }

    @Override
    public String getHelpDescription() {
        return "[item] or [item] from [container]";
    }

}
