package command;

import command.Command;
import interfaces.Edible;
import items.Item;
import textadventure.World;

public class CommandEat extends Command{

    @Override
    public String[] getCommandWords() {
        return new String[]{"eat"};
    }

    @Override
    public void doCommand(String cmd, String[] params, World world) {
        
        if(params.length == 1) {
            
            String item = params[0];
            if(!world.getPlayer().hasItem(item)) {
                World.print("You don't have the " + item + "\n\n");
        
            }else {
                Item i = world.getPlayer().getItem(item);
                if(i instanceof Edible) {
                    ((Edible)i).doEat();
                    world.getPlayer().removeItem(i);
                }else {
                    World.print("That's plainly inedible! \n\n");
                }
            }
        }else if(params.length == 0){
            World.print("Eat what? \n\n");
        }else {
            World.print("Eat one thing at a time! \n\n");
        }
        
        
    }

    @Override
    public String getHelpDescription() {
        // TODO Auto-generated method stub
        return "[item]";
    }

}
